# TFM_Evaluación de herramientas bioinformáticas para la detección de SVs y STRs a partir de datos de WGS

Evaluación de herramientas bioinformáticas para la detección de variaciones estructurales (SVs) y expansiones a partir de datos de WGS basados en short-read sequencing.